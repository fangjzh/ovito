<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="application_settings.particles"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Particle settings</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
      <imagedata fileref="images/app_settings/particle_settings.png" format="PNG" scale="80" />
    </imageobject></mediaobject></screenshot></informalfigure>     
    This page of the <link linkend="application_settings">application settings dialog</link> lets you 
    manage the program's presets for particle colors and sizes.
  </para>

  <simplesect>
    <title>Default particle colors and radii</title>
    <para>
      When importing a simulation file, OVITO automatically assigns standard colors and radii to particle types based on their name.
      This happens, in particular, for known chemical elements such as "He", "Fe" or "Si". The table shows the predefined association of named particle types 
      with corresponding default colors, display radii and van der Waals radii. You can edit the values if needed. OVITO will remember these
      default values across program sessions and apply them to newly imported datasets.
    </para>
    <para>
      Note that you can also set a new default values for invidual particle types from the <link linkend="scene_objects.particle_types">Particle types</link> panel, where you 
      typically configure the appearance of particle types after importing a simulation file. 
      Here, you can even define user-defined color and radius presets for particle types other than the standard chemical elements.
    </para>
    <para>
      Press the <guibutton>Restore built-in defaults</guibutton> button to reset all colors and radii back to the hard-coded factory default 
      values of OVITO and discard any user-defined presets made by you.
    </para>
  </simplesect>

</section>